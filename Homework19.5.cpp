﻿#include <iostream>
#include "Animal.h"
#include "Dog.h"
#include "Cat.h"
#include "Cow.h"

int main()
{
    const std::size_t length{3};

    Animal* animals[length]{ new Cat{},new Dog{},new Cow{} };

    for (int i = 0;i < length;i++) {
        animals[i]->Voice();
    }

    for (int i = 0;i < length;i++) {
        delete animals[i];
    }
}